# Nine Mens Morris AI

A project which includes a decent but not perfect Nine Men's Morris AI and a console UI which enables the user to play against said AI. The AI is determining moves using minimax algorithm with alpha-beta prunning.  

AI requires further optimisation in order to become better. The goal of this project is to demonstrate the algorithm.
