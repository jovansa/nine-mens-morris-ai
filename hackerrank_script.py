import copy

CHARS = ['O', 'W', 'B']

# HR_COORDS predstavlja reprezentaciju polja na osnovu Hackerrank zahteva
HR_COORDS = {1: '6 0', 2: '6 3', 3: '6 6', 4: '5 1', 5: '5 3', 6: '5 5', 7: '4 2', 8: '4 3', 9: '4 4',
             10: '3 0', 11: '3 1', 12: '3 2', 13: '3 4', 14: '3 5', 15: '3 6', 16: '2 2', 17: '2 3', 18: '2 4',
             19: '1 1', 20: '1 3', 21: '1 5', 22: '0 0', 23: '0 3', 24: '0 6'}

# MILLS predstavlja sva polja koja cine jedan mill
MILLS = ((1, 2, 3), (4, 5, 6), (7, 8, 9), (10, 11, 12), (13, 14, 15), (16, 17, 18), (19, 20, 21), (22, 23, 24),
         (1, 10, 22), (4, 11, 19), (7, 12, 16), (2, 5, 8), (17, 20, 23), (9, 13, 18), (6, 14, 21), (3, 15, 24))

# ADJACENT predstavlja par integer: tuple, gde integer predstavlja polje, a tuple skup svih polja koja su njemu susedna
ADJACENT = {1: (2, 10), 2: (1, 3, 5), 3: (2, 15), 4: (5, 11), 5: (2, 4, 6, 8), 6: (5, 14),
            7: (8, 12), 8: (5, 7, 9), 9: (8, 13), 10: (1, 22, 11), 11: (4, 10, 12, 19), 12: (7, 11, 16),
            13: (14, 9, 18), 14: (13, 15, 6, 21), 15: (3, 14, 24), 16: (12, 17), 17: (16, 18, 20), 18: (13, 17),
            19: (11, 20), 20: (17, 19, 21, 23), 21: (14, 20), 22: (10, 23), 23: (22, 24, 20), 24: (23, 15)}


class Board:
    """ Formira recnik od liste u kojoj se nalazi sadrzaj table; tako se svakoj poziciji alocira odgovarajuci
    karakter; 'O' ako je polje prazno, 'W' za beli token i 'B' za crni. """

    def __init__(self, b_list):
        self.player = player
        self.opponent = opponent
        self.pos = {1: b_list[0], 2: b_list[1], 3: b_list[2], 4: b_list[3], 5: b_list[4], 6: b_list[5],
                    7: b_list[6], 8: b_list[7], 9: b_list[8], 10: b_list[9], 11: b_list[10], 12: b_list[11],
                    13: b_list[12], 14: b_list[13], 15: b_list[14], 16: b_list[15], 17: b_list[16], 18: b_list[17],
                    19: b_list[18], 20: b_list[19], 21: b_list[20], 22: b_list[21], 23: b_list[22], 24: b_list[23]}

    @property
    def to_list(self):
        """ Konvertuje recnik ponovo u listu """
        output_list = []
        for key in sorted(self.pos):
            output_list.append(self.pos[key])
        return output_list

    @property
    def n_of_player_morris(self):
        """ Prebrojava sve trojke za igraca """
        global MILLS
        player_m = 0
        for i in MILLS:
            if self.pos[i[0]] == self.pos[i[1]] == self.pos[i[2]] == self.player:
                player_m += 1
        return player_m

    @property
    def n_of_opp_morris(self):
        """ Prebrojava sve trojke za protivnika """
        global MILLS
        opp_m = 0
        for i in MILLS:
            if self.pos[i[0]] == self.pos[i[1]] == self.pos[i[2]] == self.opponent:
                opp_m += 1
        return opp_m

    @property
    def morris_diff(self):
        return self.n_of_player_morris - self.n_of_opp_morris

    @property
    def n_of_player_pieces(self):
        player_p = 0
        for i in range(1, 25):
            if self.pos[i] == self.player:
                player_p += 1
        return player_p

    @property
    def n_of_opp_pieces(self):
        opp_p = 0
        for i in range(1, 25):
            if self.pos[i] == self.opponent:
                opp_p += 1
        return opp_p

    @property
    def pieces_diff(self):
        return self.n_of_player_pieces - self.n_of_opp_pieces

    @property
    def n_of_closed_player(self):
        """ Vraca broj nepomicnih figura od igraca. """
        global ADJACENT
        n = 0
        for i, j in ADJACENT.items():
            if self.pos[i] == self.opponent and all(self.pos[k] != 'O' for k in j):
                n += 1
        return n

    @property
    def n_of_closed_opp(self):
        """ Vraca broj nepomicnih figura od protivnika. """
        global ADJACENT
        n = 0
        for i, j in ADJACENT.items():
            if self.pos[i] == self.player and all(self.pos[k] != 'O' for k in j):
                n -= 1
        return n

    @property
    def closed_pieces(self):
        return self.n_of_closed_player - self.n_of_closed_opp

    @property
    def two_config(self):
        """ Proverava koliko postoji konfiguracija gde bi dodavanjem jos jedne figure doslo do trojke.
        Oduzima broj konfiguracija protivnika od broja konf. igraca i vraca razliku. """
        global MILLS
        n = 0
        for i in MILLS:
            if (self.pos[i[0]] == self.pos[i[1]] == self.player and self.pos[i[2]] == 'O') \
                    or (self.pos[i[0]] == self.pos[i[2]] == self.player and self.pos[i[1]] == 'O') \
                    or (self.pos[i[1]] == self.pos[i[2]] == self.player and self.pos[i[0]] == 'O'):
                n += 1
            elif (self.pos[i[0]] == self.pos[i[1]] == self.opponent and self.pos[i[2]] == 'O') \
                    or (self.pos[i[0]] == self.pos[i[2]] == self.opponent and self.pos[i[1]] == 'O') \
                    or (self.pos[i[1]] == self.pos[i[2]] == self.opponent and self.pos[i[0]] == 'O'):
                n -= 1
        return n

    @property
    def double_morris_diff(self):
        """ Proverava koliko postoji millova koji dele jednu figuricu. Vraca razliku takvih millova kod igraca
        i kod protivnika (podeljenu sa dva, jer se prilikom provere prolazi kroz svaki mill, a posto dva milla dele
        figuru, za istu konfiguraciju n ce se povecati za 2). """
        global MILLS
        n = 0
        for m1 in MILLS:
            if self.pos[m1[0]] == self.pos[m1[1]] == self.pos[m1[2]] == self.player:
                for m2 in MILLS:
                    if m1 == m2:
                        continue
                    if self.pos[m2[0]] == self.pos[m2[1]] == self.pos[m2[2]] == self.player:
                        if set(m1) & set(m2):
                            n += 1
            elif self.pos[m1[0]] == self.pos[m1[1]] == self.pos[m1[2]] == self.opponent:
                for m2 in MILLS:
                    if m1 == m2:
                        continue
                    if self.pos[m2[0]] == self.pos[m2[1]] == self.pos[m2[2]] == self.opponent:
                        if set(m1) & set(m2):
                            n -= 1
        return int(n / 2)

    @property
    def is_terminal(self):
        """ Proverava uslove za zavrsetak igre (ukoliko neki od igraca ima 2 figure, ili ukoliko su oba ostala na po
        3 figure)"""
        if move != 'INIT' and move != 'MILL':
            if self.n_of_player_pieces == self.n_of_opp_pieces == 3:
                return True
            elif self.n_of_player_pieces == 2 or self.n_of_player_pieces == self.n_of_closed_player:
                return -1
            elif self.n_of_opp_pieces == 2 or self.n_of_opp_pieces == self.n_of_closed_opp:
                return 1
            else:
                return False

    @property
    def evaluate(self):
        """ Evaluatorska funkcija, koja vraca pretpostavljenu vrednost za svaku tabelu. Razlicita je u zavisnosti
        od toga koja faza igre je u pitanju. """
        if move == 'INIT':
            return int(26 * self.morris_diff + 1 * self.closed_pieces
                       + 9 * self.pieces_diff + 7 * self.two_config)
        elif move == 'MOVE':
            if self.n_of_player_pieces < 4:
                return int(8 * self.two_config + 1190 * self.is_terminal)
            return int(43 * self.morris_diff + 10 * self.closed_pieces + 11 * self.pieces_diff
                       + 8 * self.double_morris_diff + 1086 * self.is_terminal)
        else:
            return int(26 * self.morris_diff + 1 * self.closed_pieces + 9 * self.pieces_diff + 7 * self.two_config)

    def next_move_p1(self, switch=False):
        """Generise listu svih mogucih poteza za INIT fazu."""
        global current_board
        output_list = []
        for i in range(1, 25):
            if self.pos[i] == 'O':
                new_board = Board(self.to_list)
                if switch:
                    new_board.pos[i] = new_board.opponent
                    output_list.append(new_board)
                else:
                    new_board.pos[i] = new_board.player
                    output_list.append(new_board)
        return output_list

    def next_move_p2(self, switch=False):
        """Generise listu svih mogucih poteza za MOVE fazu. """
        global current_board, ADJACENT
        output_list = []
        for i, j in ADJACENT.items():
            if self.pos[i] == self.player:
                for k in j:
                    self.create_new_board(i, k, switch, output_list)
        return output_list

    def next_move_p3(self, switch=False):
        """Generise listu svih mogucih poteza za FLY fazu (MOVE ali sa preostale 3 figure). """
        global current_board
        output_list = []
        for i in range(24):
            if self.pos[i] == self.player:
                for j in range(24):
                    self.create_new_board(i, j, switch, output_list)

    def create_new_board(self, i, k, switch, output_list):
        """ Koristi se za formiranje duplikata nove table u fazi MOVE.
        'i' predstavlja poziciju na kojoj je figurica odgovarajuceg igraca
        'k' predstavlja poziciju na kojoj je prazno mesto"""
        if self.pos[k] == 'O':
            new_board = Board(self.to_list)
            new_board.pos[i] = 'O'
            if not switch:
                new_board.pos[k] = self.player
                output_list.append(new_board)
            else:
                new_board.pos[k] = self.opponent
                output_list.append(new_board)

    def next_move(self, switch=False):
        """Generise sve legalne poteze u zavisnosti od toga koja je faza u pitanju. """
        global move
        if move == 'INIT':
            return self.next_move_p1(switch)
        elif move == 'MOVE':
            if self.n_of_player_pieces <= 3:
                return self.next_move_p3(switch)
            return self.next_move_p2()

    def is_empty(self, field):
        if self.pos[field] == 'O':
            return True
        return False


def has_free_adjacent(board, user_input):
    global ADJACENT
    j = ADJACENT[user_input]
    if all(board.pos[k] != 'O' for k in j):
        return False
    return True


def generate_eaten_list(board, switch=True):
    """ Generise sve legalne mogucnosti uklanjanja protivnickih figura. """
    global current_board, player, opponent
    eaten_list = []
    for n in range(1, 25):
        board_eaten = copy.deepcopy(board)
        if not switch:
            if board_eaten.pos[n] == opponent:
                if is_edible(board_eaten, n, opponent):
                    board_eaten.pos[n] = 'O'
                    eaten_list.append(board_eaten)
                else:
                    continue
        else:
            if board_eaten.pos[n] == player:
                if is_edible(board_eaten, n, player):
                    board_eaten.pos[n] = 'O'
                    eaten_list.append(board_eaten)
                else:
                    continue
    return eaten_list


def is_in_mill(board, position, color):
    global MILLS
    possible_mills = []
    for mill in MILLS:
        if position in mill:
            possible_mills.append(mill)
    for i in possible_mills:
        if all(color == board.pos[x] for x in i):
            return True
    return False


def is_edible(board, position, color):
    all_in_mill = True
    for i in range(1, 25):
        if board.pos[i] == color:
            if not is_in_mill(board, i, color):
                all_in_mill = False
                break
    if all_in_mill:
        return True
    else:
        if board.pos[position] == 'O':
            return False
        return not is_in_mill(board, position, color)


def active_mills(board):
    global MILLS
    output_list = []
    for m in MILLS:
        if board.pos[m[0]] == board.pos[m[1]] == board.pos[m[2]] != 'O':
            output_list.append(m)
    return output_list


def minimaxab(position, depth, alpha, beta, switch=False):
    global move
    if position.n_of_player_pieces > 8 and move == 'INIT':
        move = 'MOVE'

    if depth == 0 or position.is_terminal:
        return position.evaluate

    if not switch:
        max_value = -1000000
        for i in position.next_move():
            value = minimaxab(i, depth - 1, alpha, beta, True) + 18 * closed_morris(position, i)
            max_value = max(max_value, value)
            alpha = max(alpha, value)
            if beta <= alpha:
                break
        return max_value

    else:
        min_value = 1000000
        for i in position.next_move(switch=True):
            value = minimaxab(i, depth - 1, alpha, beta) + 18 * closed_morris(position, i)
            min_value = min(min_value, value)
            beta = min(beta, value)
            if beta <= alpha:
                break
        return min_value


def new_coords(board, new_move):
    """ Vraca poziciju po kojoj se razlikuju dva uzastopna poteza.
    (Sluzi da se dobiju koordinate novog poteza, a uporedjuje se sa trenutnim stanjem table). """
    for i in range(1, 25):
        if board.pos[i] != new_move.pos[i]:
            return i


def new_coords_move(board, new_move):
    """ Vraca poziciju sa koje se figura pomerila, kao i poziciju na koju se figura pomerila.
    (Sluzi da se dobiju koordinate novog poteza, a uporedjuje se sa trenutnim stanjem table). """
    pos_from = ''
    pos_to = ''
    for i in range(1, 25):
        if board.pos[i] != new_move.pos[i]:
            if board.pos[i] == player and new_move.pos[i] == 'O':
                pos_from = i
            elif board.pos[i] == 'O' and new_move.pos[i] == player:
                pos_to = i
    return pos_from, pos_to


def get_eaten_list(board):
    """ Vraca listu svih mogucih legalnih 'jedenja' """
    eaten_list = []
    for n in range(1, 25):
        board_eaten = copy.deepcopy(board)
        if board_eaten.pos[n] == opponent:
            if is_edible(board_eaten, n, opponent):
                board_eaten.pos[n] = 'O'
                eaten_list.append(board_eaten)
    return eaten_list


def get_best_minimaxab(possible_moves, switch):
    """ Vraca potez koji ima najvecu vrednost dodeljenu minimax algoritmom sa alfa i beta rezovima. """
    best_move = None
    best_value = float('-inf')
    for i in possible_moves:
        i_value = minimaxab(i, 3, -100000, 100000, switch)
        if i_value > best_value:
            best_value = i_value
            best_move = i
    return best_move


def max_eaten(eaten_list):
    """ Izdvaja potez jedenja koji ima najvecu heuristicku vrednost. """
    maximal = -9999
    maximal_board = None
    for i in eaten_list:
        if i.evaluate > maximal:
            maximal = i.evaluate
            maximal_board = i
    return maximal_board


def closed_morris(previous_board, c_board):
    """ Sluzi za detektovanje sklapanja novog milla, kako bi se vracena vrednost pomnozena odgovarajucim
    koeficijentom upotrebila u simulaciji minimax algoritma. """
    if previous_board.n_of_player_morris < c_board.n_of_player_morris:
        return 1
    elif previous_board.n_of_opp_morris < c_board.n_of_opp_morris:
        return -1
    else:
        return 0


def nextMove(given_player, given_move, given_hrboard):
    """ nextMove je funkcija koja je neophodna za Hackerrank. U zavisnosti od faze, vraca odgovarajuci string sa
    koordinatama polja. """
    if given_player == player:
        switch = True
    else:
        switch = False
    if given_move == 'INIT':
        board = Board(given_hrboard)
        possible_moves = board.next_move(switch)
        best_move = get_best_minimaxab(possible_moves, switch)
        return HR_COORDS[new_coords(board, best_move)]
    elif given_move == 'MILL':
        board = Board(given_hrboard)
        best_move = max_eaten(get_eaten_list(board))
        return HR_COORDS[new_coords(board, best_move)]
    elif given_move == 'MOVE':
        board = Board(given_hrboard)
        possible_moves = board.next_move(switch)
        best_move = get_best_minimaxab(possible_moves, switch)
        pos_from, pos_to = new_coords_move(board, best_move)
        return HR_COORDS[pos_from] + ' ' + HR_COORDS[pos_to]


player = input().strip()
if player == 'W':
    opponent = 'B'
else:
    opponent = 'W'

move = input().strip()

hrboard = []
for num in range(0, 7):
    board_line_list = []
    board_line = input().strip()
    for elem in board_line:
        if elem in CHARS:
            board_line_list.append(elem)
    hrboard = board_line_list + hrboard

current_board = Board(hrboard)
print(nextMove(player, move, hrboard))
