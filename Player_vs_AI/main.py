""" Modul iz kog se pokrece program. Sadrzi funkcije koje reprezentuju interfejs."""
import time
import board
import engine
import copy


def post_turn_update(best_move, player):
    """ Azuriranje table nakon odigranog poteza. Ova funkcija takodje vodi racuna i o tome da se dogode prelazi
    izmedju faza. """
    board.previous_board = copy.deepcopy(board.current_board)
    if not player:
        board.current_board = best_move
        try:
            print('Trenutni izgled tabele: ')
            board.current_board.print_current()
        except AttributeError:
            print('AI ne moze da pronadje legalan potez!\nIgra je gotova!\nPobednik je Igrac')
            exit(0)
    board.turn_no += 1
    if board.turn_no < 9:
        board.phase = 1
    else:
        board.phase = 2
        if board.current_board.n_of_player_pieces <= 3:
            board.phase3_ai = True
        if board.current_board.n_of_opp_pieces <= 3:
            board.phase3_player = True


def endgame_message():
    if board.current_board.is_terminal == 0.01:
        print('Igra je gotova!')
        print('Ishod je neresen!')
    elif board.current_board.is_terminal == 1:
        print('Igra je gotova!')
        print('Pobednik je AI! ')
    elif board.current_board.is_terminal == -1:
        print('Igra je gotova!')
        print('Pobednik je Igrac!')


def main():
    print("~" * 20 + " MICE " + "~" * 20)
    print("Uputstvo za igru: Kada se od vas zahteva da odigrate potez, potrebno je da unesete\n"
          "broj polja koje zelite da popunite, gledajuci od dole levo ka gore desno.\n"
          "U daljim fazama igre, zahtevace se unos dva broja, koja predstavljaju broj polja gde je figura\n"
          "koja se pomera, i broj polja gde zelimo da je pomerimo.\n"
          "Primer:\n"
          "-----------------------------------------------------------------------" + "\n"
          "Izgled table sa numeracijom polja:\n"
          "   22 --- 23 --- 24                            O - - O - - O\n"
          "   | 19 - 20 - 21 |                            | O - O - O |\n"             
          "   | | 16 17 18 | |        Unos '8' na         | | O O O | |\n"
          "10 11 12  *  13 14 15     praznoj tabli->      O O O * O O O\n"
          "   | | 7  8  9  | |        bi proizveo         | | O W O | |\n"
          "   | 4 -- 5 -- 6  |                            | O - O - O |\n"
          "   1 ---- 2 ----  3                            O - - O - - O")

    while True:
        print('-' * 75)
        board.player = input("Odaberite vasu boju ('W' = Bela, 'B' = Crna) >> ")
        if board.player not in ['W', 'w', 'B', 'b']:
            print('Neispravan unos!')
            time.sleep(1.5)
            continue
        break
    board.player = board.player.upper()
    if board.player == 'W':
        board.opponent = 'B'
    else:
        board.opponent = 'W'

    if board.player == 'W':
        print('Trenutni izgled table: ')
        board.current_board.print_current()
        while not board.current_board.is_terminal:
            board.player_turn()
            best_move = engine.get_best_move_ab(board.current_board)
            post_turn_update(best_move, player=False)
        endgame_message()

    else:
        print('Trenutni izgled table: ')
        board.current_board.print_current()
        while not board.current_board.is_terminal:
            best_move = engine.get_best_move_ab(board.current_board)
            board.current_board = best_move
            if board.current_board.n_of_opp_pieces <= 3:
                board.phase3_player = True
            print('Trenutni izgled table: ')
            try:
                board.current_board.print_current()
            except AttributeError:
                print('AI ne moze da pronadje legalan potez!\nIgra je gotova!\nPobednik je Igrac')
            if board.current_board.is_terminal:
                endgame_message()
                break
            board.player_turn()
            if board.current_board.is_terminal:
                endgame_message()
                break
            post_turn_update(best_move, player=True)


if __name__ == '__main__':
    main()
