""" Implementacija minimax algoritma sa alfa i beta rezovima, kao i funkcija za odabir najadekvatnijeg poteza"""
import board


def minimaxab(position, depth, alpha, beta, switch=False):
    """ Minimax algoritam sa alfa i beta rezovima, osim sto ima pojedine promenljive koje sluze da se u simulaciji igre
    tokom algoritma promeni faza ukoliko je to potrebno. """

    old_turn_no = board.turn_no
    old_prev_board = board.previous_board
    old_phase = board.phase

    if depth == 0 or position.is_terminal:
        return position.evaluate

    if not switch:
        max_value = -1000000
        board.turn_no += 1
        for i in position.next_move():
            if board.turn_no > 8:
                board.phase = 2
            board.previous_board = position
            value = minimaxab(i, depth - 1, alpha, beta, True)
            max_value = max(max_value, value)
            alpha = max(alpha, value)
            if beta <= alpha:
                break
        board.turn_no = old_turn_no
        board.previous_board = old_prev_board
        board.phase = old_phase
        return max_value

    else:
        min_value = 1000000
        board.turn_no += 1
        for i in position.next_move(switch=False):
            if board.turn_no > 8:
                board.phase = 2
            board.previous_board = position
            value = minimaxab(i, depth - 1, alpha, beta)
            min_value = min(min_value, value)
            beta = min(beta, value)
            if beta <= alpha:
                break

        board.turn_no = old_turn_no
        board.previous_board = old_prev_board
        board.phase = old_phase
        return min_value


def get_best_move_ab(position):
    """ Za svaki legalan naredni potez se pomocu minimaxa sa a i b rezovima odredjuje vrednost, a potom se bira
    potez sa najvecom vrednoscu. """
    depth = 2
    possible_moves = position.next_move(switch=False)
    best_move = None
    best_value = float('-inf')
    for i in possible_moves:
        i_value = minimaxab(i, depth, -100000, 100000, switch=False)
        if i_value > best_value:
            best_value = i_value
            best_move = i
    return best_move
