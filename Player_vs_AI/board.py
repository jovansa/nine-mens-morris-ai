""" Modul u sklopu kog se nalazi arhitektura projekta. Sadrzi internu reprezentaciju table pomocu klase Board,
heuristiku, funkcije za formiranje legalnih poteza, obrasce za unos poteza od strane igraca, globalne varijable i
pomocne funkcije"""
import time
import copy

""" Sve pozicije koje predstavljaju mill """
MILLS = ((1, 2, 3), (4, 5, 6), (7, 8, 9), (10, 11, 12), (13, 14, 15), (16, 17, 18), (19, 20, 21), (22, 23, 24),
         (1, 10, 22), (4, 11, 19), (7, 12, 16), (2, 5, 8), (17, 20, 23), (9, 13, 18), (6, 14, 21), (3, 15, 24))

""" Sve susedne pozicije za datu poziciju """
ADJACENT = {1: (2, 10), 2: (1, 3, 5), 3: (2, 15), 4: (5, 11), 5: (2, 4, 6, 8), 6: (5, 14),
            7: (8, 12), 8: (5, 7, 9), 9: (8, 13), 10: (1, 22, 11), 11: (4, 10, 12, 19), 12: (7, 11, 16),
            13: (14, 9, 18), 14: (13, 15, 6, 21), 15: (3, 14, 24), 16: (12, 17), 17: (16, 18, 20), 18: (13, 17),
            19: (11, 20), 20: (17, 19, 21, 23), 21: (14, 20), 22: (10, 23), 23: (22, 24, 20), 24: (23, 15)}


class Board:
    """ Formira recnik od liste u kojoj se nalazi sadrzaj izgleda table; tako se svakoj poziciji alocira odgovarajuci
    karakter 'O' ako je polje prazno, 'W' za beli token i 'B' za crni. """

    def __init__(self, b_list):
        self.player = opponent
        self.opponent = player
        self.pos = {1: b_list[0], 2: b_list[1], 3: b_list[2], 4: b_list[3], 5: b_list[4], 6: b_list[5],
                    7: b_list[6], 8: b_list[7], 9: b_list[8], 10: b_list[9], 11: b_list[10], 12: b_list[11],
                    13: b_list[12], 14: b_list[13], 15: b_list[14], 16: b_list[15], 17: b_list[16], 18: b_list[17],
                    19: b_list[18], 20: b_list[19], 21: b_list[20], 22: b_list[21], 23: b_list[22], 24: b_list[23]}

    @property
    def to_list(self):
        """ Konvertuje recnik ponovo u listu """
        output_list = []
        for key in sorted(self.pos):
            output_list.append(self.pos[key])
        return output_list

    @property
    def n_of_player_morris(self):
        """ Prebrojava sve trojke za igraca """
        global MILLS
        player_m = 0
        for i in MILLS:
            if self.pos[i[0]] == self.pos[i[1]] == self.pos[i[2]] == self.player:
                player_m += 1
        return player_m

    @property
    def n_of_opp_morris(self):
        """ Prebrojava sve trojke za protivnika """
        global MILLS
        opp_m = 0
        for i in MILLS:
            if self.pos[i[0]] == self.pos[i[1]] == self.pos[i[2]] == self.opponent:
                opp_m += 1
        return opp_m

    @property
    def morris_diff(self):
        """ Razlika za ova dva je jedan od parametara koji ide u evaluatorsku funkciju. """
        return self.n_of_player_morris - self.n_of_opp_morris

    @property
    def n_of_player_pieces(self):
        player_p = 0
        for i in range(1, 25):
            if self.pos[i] == self.player:
                player_p += 1
        return player_p

    @property
    def n_of_opp_pieces(self):
        opp_p = 0
        for i in range(1, 25):
            if self.pos[i] == self.opponent:
                opp_p += 1
        return opp_p

    @property
    def pieces_diff(self):
        """ Razlika za ova dva je jedan od parametara koji ide u evaluatorsku funkciju. """
        return self.n_of_player_pieces - self.n_of_opp_pieces

    @property
    def closed_morris(self):
        """ Proverava da li postoji razlika u broju zatvorenih millova, kao i da li je mozda doslo do zatvaranja
        novog milla pomeranjem figurice sa starog. Jedan od parametara za evaluatorsku funkciju. """
        if previous_board.n_of_player_morris < self.n_of_player_morris:
            return 1
        elif previous_board.n_of_opp_morris < self.n_of_opp_morris:
            return -1
        else:
            return 0

    @property
    def n_of_closed_player(self):
        """ Vraca broj zatvorenih figura od igraca. """
        global ADJACENT
        n = 0
        for i, j in ADJACENT.items():
            if self.pos[i] == self.opponent and all(self.pos[k] != 'O' for k in j):
                n += 1
        return n

    @property
    def n_of_closed_opp(self):
        """ Vraca broj zatvorenih figura od protivnika. """
        global ADJACENT
        n = 0
        for i, j in ADJACENT.items():
            if self.pos[i] == self.player and all(self.pos[k] != 'O' for k in j):
                n -= 1
        return n

    @property
    def closed_pieces(self):
        """ Razlika ova dva je jedan od parametara za evaluatorsku funkciju"""
        return self.n_of_closed_player - self.n_of_closed_opp

    @property
    def two_config(self):
        """ Proverava koliko postoji konfiguracija gde bi dodavanjem jos jedne figure doslo do trojke.
        Oduzima broj konfiguracija protivnika od broja konf. igraca i vraca razliku. """
        global MILLS
        n = 0
        for i in MILLS:
            if (self.pos[i[0]] == self.pos[i[1]] == self.player and self.pos[i[2]] == 'O') \
                    or (self.pos[i[0]] == self.pos[i[2]] == self.player and self.pos[i[1]] == 'O') \
                    or (self.pos[i[1]] == self.pos[i[2]] == self.player and self.pos[i[0]] == 'O'):
                n += 1
            elif (self.pos[i[0]] == self.pos[i[1]] == self.opponent and self.pos[i[2]] == 'O') \
                    or (self.pos[i[0]] == self.pos[i[2]] == self.opponent and self.pos[i[1]] == 'O') \
                    or (self.pos[i[1]] == self.pos[i[2]] == self.opponent and self.pos[i[0]] == 'O'):
                n -= 1
        return n

    @property
    def double_morris_diff(self):
        """ Racuna razliku izmedju broja igracevih duplih millova u kojima je jedna figurica ista,
        i protivnikovih. """
        global MILLS
        n = 0
        for m1 in MILLS:
            if self.pos[m1[0]] == self.pos[m1[1]] == self.pos[m1[2]] == self.player:
                for m2 in MILLS:
                    if m1 == m2:
                        continue
                    if self.pos[m2[0]] == self.pos[m2[1]] == self.pos[m2[2]] == self.player:
                        if set(m1) & set(m2):
                            n += 1
            elif self.pos[m1[0]] == self.pos[m1[1]] == self.pos[m1[2]] == self.opponent:
                for m2 in MILLS:
                    if m1 == m2:
                        continue
                    if self.pos[m2[0]] == self.pos[m2[1]] == self.pos[m2[2]] == self.opponent:
                        if set(m1) & set(m2):
                            n -= 1
        return int(n / 2)

    @property
    def is_terminal(self):
        """ Proverava uslove za zavrsetak igre (ukoliko neki od igraca ima 2 figure, ili ukoliko su oba ostala na po
        3 figure)"""
        if phase != 1:
            if self.n_of_player_pieces == self.n_of_opp_pieces == 3:
                return 0.01
            elif self.n_of_player_pieces == 2 or self.n_of_player_pieces == self.n_of_closed_player:
                return -1
            elif self.n_of_opp_pieces == 2 or self.n_of_opp_pieces == self.n_of_closed_opp:
                return 1
            else:
                return False

    @property
    def evaluate(self):
        """ Evaluatorska funkcija."""
        if phase == 1:
            return int(28 * self.closed_morris + 26 * self.morris_diff + 1 * self.closed_pieces + 9 * self.pieces_diff
                       + 10 * self.two_config)
        elif phase == 2:
            if (player == self.player and phase3_player) or (player == self.opponent and phase3_ai):
                return int(16 * self.closed_morris + 10 * self.two_config + 1190 * self.is_terminal)
            return int(24 * self.closed_morris + 43 * self.morris_diff + 10 * self.closed_pieces + 11 * self.pieces_diff
                       + 8 * self.double_morris_diff + 1086 * self.is_terminal)

#  Slede funkcije koje generisu sve moguce legalne poteze u odnosu na fazu.
    def next_move_p1(self, switch=False):
        global current_board
        output_list = []
        for i in range(1, 25):
            if self.pos[i] == 'O':
                new_board = Board(self.to_list)
                if switch:
                    new_board.pos[i] = new_board.opponent
                    append_board(output_list, new_board, switch=True)
                else:
                    new_board.pos[i] = new_board.player
                    append_board(output_list, new_board)
        return output_list

    def next_move_p2(self, switch=False):
        global current_board, ADJACENT
        output_list = []
        for i, j in ADJACENT.items():
            if self.pos[i] == self.player:
                for k in j:
                    self.create_new_board(i, k, switch, output_list)
        return output_list

    def next_move_p3(self, switch=False):
        global current_board
        output_list = []
        for i in range(24):
            if self.pos[i] == self.player:
                for j in range(24):
                    self.create_new_board(i, j, switch, output_list)

    def create_new_board(self, i, k, switch, output_list):
        """ Koristi se za formiranje duplikata nove table u fazi 2.
        'i' predstavlja poziciju na kojoj je figurica odgovarajuceg igraca
        'k' predstavlja poziciju na kojoj je prazno mesto"""
        if self.pos[k] == 'O':
            new_board = Board(self.to_list)
            new_board.pos[i] = 'O'
            if not switch:
                new_board.pos[k] = self.player
                append_board(output_list, new_board)
            else:
                new_board.pos[k] = self.opponent
                append_board(output_list, new_board, switch=True)

    def next_move(self, switch=False):
        """ Funkcija koja sjedinjuje sve generatore poteza, i vraca potez u odnosu na fazu. """
        global phase
        if phase == 1:
            return self.next_move_p1(switch)
        elif phase == 2:
            if (self.player == player and phase3_player) or (self.opponent == opponent and phase3_ai):
                return self.next_move_p3(switch)
            return self.next_move_p2()

    def is_empty(self, field):
        if self.pos[field] == 'O':
            return True
        return False

    def print_current(self):
        print(self.pos[22] + ' - - ' + self.pos[23] + ' - - ' + self.pos[24])
        print('| ' + self.pos[19] + ' - ' + self.pos[20] + ' - ' + self.pos[21] + ' |')
        print('| | ' + self.pos[16] + ' ' + self.pos[17] + ' ' + self.pos[18] + ' | |')
        print(self.pos[10] + ' ' + self.pos[11] + ' ' + self.pos[12] + ' * ' + self.pos[13] + ' ' + self.pos[14] + ' '
              + self.pos[15])
        print('| | ' + self.pos[7] + ' ' + self.pos[8] + ' ' + self.pos[9] + ' | |')
        print('| ' + self.pos[4] + ' - ' + self.pos[5] + ' - ' + self.pos[6] + ' |')
        print(self.pos[1] + ' - - ' + self.pos[2] + ' - - ' + self.pos[3])


def has_free_adjacent(board, user_input):
    """ Proverava da li polje uneto od strane korisnika ima slobodno susedno polje. (Koristi se prilikom pomeranja
    u fazi 2. """
    global ADJACENT
    j = ADJACENT[user_input]
    if all(board.pos[k] != 'O' for k in j):
        return False
    return True


def player_turn():
    global previous_board, current_board, player, phase
    if phase == 1:
        player_turn_p1()
        remove_opponent_man()
    elif phase == 2:
        player_turn_p2()
        remove_opponent_man()


def player_turn_p1():
    global previous_board, current_board, player
    while True:
        turn = input('Unesite Vas potez >> ')
        try:
            if int(turn) < 0 or int(turn) > 24:
                print('Neispravan unos!')
                time.sleep(1.5)
                continue
        except ValueError:
            print('Neispravan unos!')
            time.sleep(1.5)
            continue
        turn = int(turn)
        if not current_board.is_empty(turn):
            print('Polje je popunjeno!')
            time.sleep(1.5)
            continue
        previous_board = copy.deepcopy(current_board)
        current_board.pos[turn] = player
        print('Trenutni izgled tabele: ')
        current_board.print_current()
        break


def player_turn_p2():
    global previous_board, current_board, player
    while True:
        move_from = input('Unesite polje na kom je figura koju zelite da pomerite >> ')
        try:
            move_from = int(move_from)
        except ValueError:
            print('Neispravan unos!')
            time.sleep(1.5)
            continue
        try:
            if current_board.pos[move_from] != player:
                print('Na tom polju nije Vasa figura!')
                time.sleep(2)
                continue
            if not has_free_adjacent(current_board, move_from):
                print('Odabrana figura se ne moze pomeriti!')
                time.sleep(2)
                continue
        except KeyError:
            print('Neispravan unos!')
            time.sleep(1.5)
            continue
        move_from = int(move_from)
        break
    while True:
        move_to = input('Unesite polje na koje zelite da pomerite figuru >> ')
        try:
            move_to = int(move_to)
        except ValueError:
            print('Neispravan unos!')
            time.sleep(1.5)
            continue
        try:
            if not phase3_player:
                if current_board.pos[move_to] != 'O' or move_to not in ADJACENT[move_from]:
                    print('Na odabrano polje ne mozete staviti figuru!')
                    time.sleep(1.5)
                    print('Pokusajte neko drugo polje!')
                    time.sleep(1.5)
                    continue
            else:
                if current_board.pos[move_to] != 'O':
                    print('Na odabrano polje ne mozete staviti figuru!')
                    time.sleep(1.5)
                    print('Pokusajte neko drugo polje!')
                    time.sleep(1.5)
                    continue
        except KeyError:
            print('Neispravan unos!')
            time.sleep(1.5)
            continue
        break
    previous_board = copy.deepcopy(current_board)
    current_board.pos[move_from] = 'O'
    current_board.pos[move_to] = player
    print('Trenutni izgled tabele: ')
    current_board.print_current()


def remove_opponent_man():
    global previous_board, current_board, opponent
    if current_board.n_of_opp_morris > previous_board.n_of_opp_morris or \
            (len(active_mills(previous_board)) == len(active_mills(current_board)) and
             active_mills(previous_board) != active_mills(current_board)):
        while True:
            remove_from = input('Unesite polje sa kog zelite da uklonite protivnicku figuru >> ')
            try:
                remove_from = int(remove_from)
            except ValueError:
                print('Neispravan unos!')
                time.sleep(1.5)
                continue
            try:
                if current_board.pos[int(remove_from)] != opponent or \
                        not is_edible(current_board, remove_from, opponent):
                    print('Sa te pozicije ne mozete ukloniti figuru!')
                    time.sleep(1.5)
                    print('Pokusajte ponovo!')
                    time.sleep(1.5)
                    continue
            except KeyError:
                print('Neispravan unos!')
                time.sleep(1.5)
                continue
            break
        current_board.pos[remove_from] = 'O'
        print('Trenutni izgled tabele: ')
        current_board.print_current()


def generate_eaten_list(board, switch=True):
    """ Generise listu svih legalnih mogucnosti uklanjanja protivnickih figurica. """
    global current_board, player, opponent
    eaten_list = []
    for n in range(1, 25):
        board_eaten = copy.deepcopy(board)
        if not switch:
            if board_eaten.pos[n] == opponent:
                if is_edible(board_eaten, n, opponent):
                    board_eaten.pos[n] = 'O'
                    eaten_list.append(board_eaten)
                else:
                    continue
        else:
            if board_eaten.pos[n] == player:
                if is_edible(board_eaten, n, player):
                    board_eaten.pos[n] = 'O'
                    eaten_list.append(board_eaten)
                else:
                    continue
    return eaten_list


def is_in_mill(board, position, color):
    global MILLS
    possible_mills = []
    for mill in MILLS:
        if position in mill:
            possible_mills.append(mill)
    for i in possible_mills:
        if all(color == board.pos[x] for x in i):
            return True
    return False


def is_edible(board, position, color):
    all_in_mill = True
    for i in range(1, 25):
        if board.pos[i] == color:
            if not is_in_mill(board, i, color):
                all_in_mill = False
                break
    if all_in_mill:
        return True
    else:
        if board.pos[position] == 'O':
            return False
        return not is_in_mill(board, position, color)


def active_mills(board):
    global MILLS
    output_list = []
    for m in MILLS:
        if board.pos[m[0]] == board.pos[m[1]] == board.pos[m[2]] != 'O':
            output_list.append(m)
    return output_list


def append_board(board_list, new_board, switch=False):
    """ Funkcija koja se koristi za appendovanje listi koje su rezultat generisanja poteza. Ona se koristi zbog toga
    sto ako dodje do jedenja, treba dodati i jos jednu listu poteza koje predstavljaju sve legalne mogucnosti
    uklanjanja. """
    global current_board
    if not switch:
        if new_board.n_of_player_morris > current_board.n_of_player_morris:
            eaten_list = generate_eaten_list(new_board)
            board_list += eaten_list
        else:
            board_list.append(new_board)
    else:
        if new_board.n_of_opp_morris > current_board.n_of_opp_morris:
            eaten_list = generate_eaten_list(new_board, switch=False)
            board_list += eaten_list
        else:
            board_list.append(new_board)


player = ''
opponent = ''
phase = 1
phase3_player, phase3_ai = False, False
previous_board = Board(['O'] * 24)
current_board = Board(['O'] * 24)
turn_no = 0
